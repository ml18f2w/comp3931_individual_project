# COMP3931_Individual_Project

Name: Fengyun Wang 
Email address: ml18f2w@leeds.ac.uk

The program code of my Individual Project.

Application Deployment：

Firstly you need to download the code for the project from Project_Program_Code and second you need to download the Java version of Word2Vec
from the Word2Vec model directory.

This project references the Java version of Word2Vec, so it is necessary to add a Maven reference to the pom.xml when deploying the project:
<dependency>
    <groupId>com.ansj</groupId>
    <artifactId>Word2VEC_java</artifactId>
    <version>0.0.1-SNAPSHOT</version>
</dependency>

Thridly, this project references Google Pre-Traded Vectors, which needs to be downloaded from the following URL:
https://code.google.com/archive/p/word2vec/


